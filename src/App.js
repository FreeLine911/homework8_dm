import { useState, useEffect } from "react";
import './App.css';

function App() {
    const [albumId, setAlbumId] = useState(0);
    const [albums, setAlbums] = useState([]);
    const [photos, setPhotos] = useState([]);


    const onClick = (albumId) => (e) => {
        setAlbumId(albumId);
        getData(albumId);
    }


    useEffect (() => {
        fetch("https://jsonplaceholder.typicode.com/albums")
            .then((response) => response.json())
            .then((data) => {
                setAlbums(data);
            });
getData(albumId);
    }, []);

    function getData(id){
        fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
            .then((response) => response.json())
            .then((data) => {
                setPhotos(data);
            });
    }


    return (
        <div className="gallery">
            <aside className="albums">
                <h4 className="albums__title">Albums</h4>
                <ul className="albums-list">
                    {albums.map(({ id, title }) => {
                        let classes = "albums-list__item";

                        if (albumId === id) {

                            classes += " albums-list__item--active";
                        }

                        return (

                            <li key={id} className={classes} onClick={onClick(id)}>{title}</li>

                        );
                    })}
                </ul>
            </aside>

            <div>
                {photos.map(({url}) => {
                    return (
                        <li><img src = {url}/></li>


                    );
                })}
            </div>

            <div>
            <main className="photos" />
            </div>
        </div>
    );
}

export default App;
